\chapter{Arhitektura i dizajn sustava}

Ova web aplikacija dizajnirana je malo modificiranom Model-View-Controller arhitekturom. Takva arhitektura pokazala se pogodnom jer odvaja ključne dijelove aplikacije na zasebne cjeline. Time je puno jednostavnije mijenjati i dodavati nove značajke u aplikaciju, jer nove značajke neće (uglavnom) pokvratiti prethodni kod (bit će \textit{backwards-compatible}). Aplikacije je pisana u Pythonu pomoću biblioteke Flask pa sadržava i objektno-orijentirani i funkcionalni pristup MVC arhitekturi.

Tako, primjerice, Flask Blueprintovi predstavljaju \textit{Controller} dio MVC arhitekture, Jinja templateovi \textit{View} dio, a Python funkcije za pristup bazi \textit{Model} dio.

U klasičnoj MVC arhitekturi Model dio predstavljaju klase koje najčešće preslikavaju strukturu tablice u bazi podataka u skup atributa klase. Za te su arhitekture Modeli čisti skupovi podataka (\textit{plain-old-data}) koji imaju svoje gettere i settere. Time postoje zasebne klase koje povezuju pristup bazi podataka sa procesom kreiranja i dohvaćanja podataka iz baze i preslikavanja istih u Modele (klase). U programskom jeziku Java, konkretno frameworku \textit{Spring} to se zove \textit{Data Access Object (DAO)}.

Mi smo ovdje poduzeli malo drugačiji pristup i iskoristili jednostavnost Pythonovih postojećih modula na način objašnjen u poglavlju Baza podataka.

\begin{figure}
\includegraphics[width=300pt]{"dijagrami/arhitektura.png"}
\centering
\caption{Dijagram arhitekture}
\end{figure}


\section{Baza podataka}

Aplikacija koristi SQLite bazu podataka. SQLite je baza podataka koja se najčešće koristi u \textit{embedded} sustavima jer je mala, jednostavna, minimalna i ne zahtijeva da postoji server koji daje pristup bazi, nego je baza jednostavna datoteka na disku. No, SQLite se može sasvim pouzdano koristiti u manjim i osrednjim web projektima gdje nisu potrebne napredne značajke baza podataka poput PostgreSQL, MySQL itd.

U većim web projektima, SQLite postaje problematičan jer veći projekti zahtijevaju česti paralelni pristup bazi, odnosno u SQLite-u česti paralelni pristup jednoj datoteci na disku. Time SQLite zaključava i otključava bazu pri pisanju i čitanju što postaje sporo. To nužno nije presporo za rad velike web aplikacije, pa je time potrebno provesti dodatna mjerenja da bi se ustanovilo hoće li SQLite predstavljati problem. U našem slučaju to nije bio problem (jer se radi o manjem projektu), a još jedna prednost jest što Pythonova standardna biblioteka već sadržava modul za rad sa sqlite-om pa nije potrebno koristiti dodatne pakete.

Naime, sqlite modul u Pythonu pri svakom pristupu bazi vraća Row objekt koji se može koristit kao Python rječnik (dictionary). Primjerice:

\begin{lstlisting}[language=Python]
import sqlite3

# Uspostava veze na bazu podataka
db = sqlite3.connect('data.db')
db.row_factory = sqlite3.Row

c = db.cursor()

# Koristimo tzv. SQL bindinge kako bi se izbjegao
# SQL-injection napad
c.execute('SELECT ime, prezime FROM users WHERE id=?', (1,))

osoba = c.fetchone()

# Ispisuje vrijednosti iz baze
print(osoba['ime'], osoba['prezime'])
\end{lstlisting}

Time su naši Row objekti arhitekturalno gledano Modeli. Puno jezika ima slične pristupe ovom, primjerice, Java ima sličan pristup pomoću cursora gdje se do podataka može doći indeksom ili imenom stupca, no to u takvim jezicima nije idiomatično za objektno-usmjereno pisanje koda. U Pythonu, koji je dinamički jezik, ovakav pristup je potpuno prihvatljiv i jednostavan.

\subsection{Opis tablica}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{accounts}}             \\ \hline
		\cellcolor{LightGreen}id & INTEGER &                \\ \hline
		        username         &  TEXT   & korisničko ime \\ \hline
		        password         &  TEXT   &    lozinka     \\ \hline
	\end{tabular}
	\caption{\textit{accounts}}
	\centering
\end{table}


\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{users}}                                                       \\ \hline
		\cellcolor{LightGreen}id & INTEGER &                                                       \\ \hline
		          name           &  TEXT   &                          ime                          \\ \hline
		        surname          &  TEXT   &                        prezime                        \\ \hline
		         email           &  TEXT   &                         email                         \\ \hline
		          type           &  TEXT   & 'user' za obične korisnike, 'admin' za administratore \\ \hline
	\end{tabular}
	\caption{\textit{users}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{sellers}}                           \\ \hline
		\cellcolor{LightGreen}id & INTEGER &                             \\ \hline
		          oib            &  TEXT   &        OIB trgovine         \\ \hline
		          name           &  TEXT   &       naziv trgovine        \\ \hline
		   num\_wrong\_prices    & INTEGER & broj krivo navedenih cijena \\ \hline
	\end{tabular}
	\caption{\textit{sellers}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{tags}}           \\ \hline
		\cellcolor{LightGreen}id & INTEGER &          \\ \hline
		          name           &  TEXT   & ime taga \\ \hline
	\end{tabular}
	\caption{\textit{tags}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{products}}                           \\ \hline
		\cellcolor{LightGreen}id & INTEGER &                              \\ \hline
		          name           &  TEXT   &       naziv proizvoda        \\ \hline
		      date\_added        &  TEXT   & datum kada je proizvod dodan \\ \hline
	\end{tabular}
	\caption{\textit{products}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{seller\_products}}                                         \\ \hline
		\cellcolor{LightGreen}product\_id & INTEGER &          identifikator proizvoda          \\ \hline
		\cellcolor{LightGreen}seller\_id  & INTEGER &          identifikator trgovine           \\ \hline
		              price               & INTEGER & cijena koju trgovina nudi za taj proizvod \\ \hline
	\end{tabular}
	\caption{\textit{seller\_products}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{tag\_suggestions}}                       \\ \hline
		 \cellcolor{LightGreen}user\_id   & INTEGER & identifikator proizvoda \\ \hline
		\cellcolor{LightGreen}product\_id & INTEGER & identifikator trgovine  \\ \hline
		  \cellcolor{LightGreen}tag\_id   & INTEGER &   identifikator taga    \\ \hline
	\end{tabular}
	\caption{\textit{tag\_suggestions}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{banned\_accounts}}                                 \\ \hline
		\cellcolor{LightGreen}account\_id & INTEGER &      identifikator korisnika      \\ \hline
		  \cellcolor{LightGreen}reason    &  TEXT   & razlog zašto je korisnik blokiran \\ \hline
	\end{tabular}
	\caption{\textit{banned\_accounts}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{price\_changes}}                                                               \\ \hline
		        \cellcolor{LightGreen}id         & INTEGER &                 identifikator zahtjeva                 \\ \hline
		     \cellcolor{LightBlue}user\_id       & INTEGER &    identifikator korisnika koji je napravio zahtjev    \\ \hline
		\cellcolor{LightBlue}seller\_product\_id & INTEGER &            identifikator seller\_product-a             \\ \hline
		                 proof                   &  BLOB   &             dokaz o krivoj cijeni (slika)              \\ \hline
		               new\_price                & INTEGER &             cijena koju korisnik predlaže              \\ \hline
		   \cellcolor{LightBlue}approved\_by     & INTEGER & identifikator administratora koji je prihvatio zahtjev \\ \hline
		             date\_approved              &  TEXT   &            datum kada je zahtjev prihvaćen             \\ \hline
	\end{tabular}
	\caption{\textit{price\_changes}}
	\centering
\end{table}

\begin{table}[H]
	\begin{tabular}{|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{\textbf{comments}}                                                           \\ \hline
		   \cellcolor{LightGreen}id     & INTEGER &                                                       \\ \hline
		\cellcolor{LightBlue}seller\_id & INTEGER &   identifikator trgovine na koju se odnosi komentar   \\ \hline
		 \cellcolor{LightBlue}user\_id  & INTEGER & identifikator administratora koji je napisao komentar \\ \hline
		            comment             &  TEXT   &                    tekst komentara                    \\ \hline
	\end{tabular}
	\caption{\textit{comments}}
	\centering
\end{table}


\eject

\subsection{Dijagram baze podataka}
\begin{figure}
\includegraphics[angle=90,width=\textwidth]{"dijagrami/database.png"}
\caption{Dijagram baze podataka}
\end{figure}


\eject


\section{Dijagram razreda}
Odabirom \href{https://palletsprojects.com/p/flask/}{Flask}
mikro web okvira aplikacija je nativno kombinacija funkcionalnog i proceduralnog
stila programiranja i ne podliježe uvaženom MVC modelu koji je prezentiran u okviru kolegija.
Iako nepotrebni, razredi su korišteni za organiziranje ruta i isporuku podataka
frontendu Jinja2 template procesorom \href{https://jinja.palletsprojects.com/en/2.10.x/}{Jinja2}
koji, također minimalan po dizajnu, generira HTML dostavljen klijentu.

\bigskip

S obzirom na to da se radi o Pythonu, prirodnije je govoriti o modulima i paketima, a ne razredima.
Dijelovi aplikacije kategorizirani u pakete po zahtjevima tipova korisnika uz
specijalan skup obrazaca za grupiranje proizvoda. Podno uvodnog teksta,
4 od 5 Python paketa opisani su dijagramima razreda.
Peti paket, korišten za komunikaciju s bazom podataka, izostavljen je 
zbog jednostavnosti i prirode tehnologije te je implementiran isključivo 
koristeći slobodne funkcije.

\bigskip

Nešto ranije spomenuta uloga razreda u organizaciji ruta izvršena 
je nasljeđivanjem Flask nativnih razreda View i MethodView.
Detaljniju dokumentaciju možete pronaći ovdje: \href{https://flask.palletsprojects.com/en/1.1.x/views/}{Pluggable Views}.
U dijagramima uočljiv je još nadrazred \href{https://flask-wtf.readthedocs.io/en/stable/}{FlaskForm} korišten
za jednostavan i siguran prijenos podataka pomoću CSRF-Tokena.

\bigskip

	\subsection{Korisnički računi}
		\includegraphics[scale=0.195]{dijagrami/razredi/accounts.png}

	\subsection{Administrator}
		\includegraphics[scale=0.225]{dijagrami/razredi/admin.png}

	\subsection{Autentifikacija}
		\includegraphics[scale=0.125]{dijagrami/razredi/auth.png}

	\subsection{Proizvodi}
		\includegraphics[scale=0.095]{dijagrami/razredi/products.png}

\eject

\section{Dijagram stanja}

Dijagram stanja prikazuje stanja objekta te prijelaze iz jednog stanja u drugo temeljene na događajima. Na slici 4.3. je  prikazan dijagram stanja za klijenta. Nakon prijave, klijentu se prikazuje početna stranica na kojoj može potražiti proizvode koji ga zanimaju. Kada odabere proizvod koji ga zanima klijent može vidjeti sve podatke o proizvodu, trgovine koje taj proizvod prodaju, te može prijaviti neispravnu cijenu proizvoda. Ako klijent odabere ime trgovine koja je navedena na stranici profila proizvoda prikazat će mu se informacije o trgovini. Ako klijent želi prijaviti neispravnu cijenu proizvoda morat će ispuniti formu za prijavu. Nakon što pošalje ispravno popunjenu formu klijent se vraća na stranicu profila proizvoda. Također, klijent može u padajućem izborniku odabrati neku od ponuđenih mogućnosti. Klikom na ime profila prikazuju mu se njegovi podaci kao i opcije za uređenje postavki privatnosti. Klikom na „Odjava“ klijent se odjavljuje.\\

\begin{figure}[H]
\includegraphics[width=\textwidth]{dijagrami/dijagram_stanja.jpg}
\centering
\caption{Dijagram stanja}
\end{figure}


\eject

\section{Dijagram aktivnosti}

Dijagram aktivnosti primjenjuje se za opis modela toka upravljanja ili toka podataka. Ne upotrebljava se za modeliranje događajima poticanog ponašanja. U modeliranju toka upravljanja svaki novi korak poduzima se nakon završenog prethodnog, a naglasak je na jednostavnosti. Na slici 4.4. prikazan je proces prijave neispravne cijene. Korisnik se prijavi u sustav, odabere proizvod za koji želi prijaviti neispravnu cijenu. Nakon što klijent klikne na gumb za prijavu cijene prikazuje mu se forma u koju mora unijeti dokaz o krivoj cijeni. Nakon toga se prijava evidentira u bazi podataka, te se pošalje obavijest administratoru koji treba donijeti odluku o valjanosti dokaza. Nakon što je administrator unio svoju odluku prikaz web-aplikacija na profilu proizvoda i trgovine se prikladno osvježi.\\

\begin{figure}[H]
\includegraphics[width=420pt]{dijagrami/dijagram_aktivnosti.jpg}
\centering
\caption{Dijagram aktivnosti}
\end{figure}

\eject
\section{Dijagram komponenti}

Sivo područje unutar dijagrama komponenti prikazuje središnju aplikaciju sastavljenu
od dvije usko povezane komponente. Flask backend aplikacije koja na HTTP zahtjeve odgovara
HTML stranicama generiranim Jinja2 template procesorom zaduženim za frontend uz 
\href{https://getbootstrap.com}{Bootstrap 4} okvir. 
Struktura Python paketa podudara se s template hijerarhijom u Jinja2 static folderima.
Dizajn je diktiran routing sistemom zasnovanim na Flask \href{https://flask.palletsprojects.com/en/1.1.x/tutorial/views/}{Blueprint}u.
Ruter nije prikazan kao zasebna komponenta u dijagram jer pripada Flask web okviru i duboko je povezan s njegovom strukturom, 
a opisivanje unutarnje arhitekture Flaska je van ovog dokumenta.

API za komuniciranje s bazom podataka oblikovan je \href{https://docs.python.org/3/library/sqlite3.html}{sqlite3} modulom
koji je dio Python3 standarda. Uobičajeni DTO komponenta je također izostavljena jer se prijenos 
podataka vrši nativno pomoću Python \href{https://docs.python.org/3/tutorial/datastructures.html#dictionaries}{dictionary}a,
a tradicionalni REST-api je izostavljen zbog Flask 
\href{https://flask.palletsprojects.com/en/1.1.x/quickstart/#rendering-templates}{render\_template} dizajn obrasca.

Rozo obojene komponente predstavljaju vanjske pakete instalirane \href{https://pypi.org/project/pip/}{pip-om}
korištene u izradi aplikacije. \href{https://flask-wtf.readthedocs.io/en/stable/}{Flask-WTF} 
Pruža jednostavno i sigurno sučelje za kreiranje HTML formi osigurava protekciju protiv
 \href{https://en.wikipedia.org/wiki/Cross-site_request_forgery}{Cross-site request forgery} napada.
 \href{https://click.palletsprojects.com/en/7.x/}{Click} je zadužen za kreiranje CLI naredbi
 korisnih administratoru za inicijaliziranje baze podataka.

Bootstrap, prikazan ljubičastom, pruža jednostavno i brzo sučelje za izgradnju 
\href{https://developers.google.com/web/fundamentals/design-and-ux/responsive}{responsive} aplikacija
što je pogodno za izgradnju brzih prototipa, studentskih projekata i generalno agilno orijentirano razvoja proizvoda.

Shell konzola (zeleno) i klijent Web preglednik (crveno) iako ne bi trebali biti dio dijagram,
zadržani su radi boljeg prikaza povezanosti klijen-server okruženja s okolinom.

\bigskip

\includegraphics[scale=0.3]{dijagrami/dijagram_komponenti.png}