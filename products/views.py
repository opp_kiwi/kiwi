from flask import Blueprint, redirect, render_template, request, session, url_for, make_response, jsonify, send_file
from flask.views import MethodView

from ..database import product, seller_product, tags, price_changes
from .. import auth
from ..commons import RenderTemplateView, PostGetFormView, SearchForm, create_product_search_form
from .. auth import require_levels

from . import forms
import csv
import io

blueprint = Blueprint('products', __name__, url_prefix='/products')


class ProductsIndex(RenderTemplateView):
    def __init__(self):
        super(ProductsIndex, self).__init__(
            'products/edit.html', products=product.all_min_price())


class NewProductView(PostGetFormView):
    decorators = [lambda f: require_levels(f, ('seller',))]

    def __init__(self):
        super(NewProductView, self).__init__(
            forms.NewProductForm(), 'products/new.html')

        def to_id_name_pairs(row):
            return (row['id'], row['name'])

        self.form.existing_product.choices = [
            (-1, 'Odaberi...'), *map(to_id_name_pairs, product.all())]

    def view_post_logic(self):
        d = self.form.data
        if d['use_existing']:
            product_id = d['existing_product']
        else:
            product_id = product.new(d['name'])
        price = round(d['price'] * 100)
        seller_product.new(product_id, auth.current_user(), price)

        return redirect(url_for('products.editor'))


class ProductsEditorView(MethodView):
    decorators = [lambda f: require_levels(f, ('seller',))]

    ''' Manages logic for viewing and deleting seller product catalog '''
    methods = ['GET']

    def __init__(self):
        self.product_search_form = create_product_search_form()

    def get(self, get_products_fn=lambda seller_id: seller_product.all_from(seller_id)):
        products = get_products_fn(auth.current_user())
        top_tags = tags.get_top_tags_products(products)

        return render_template(
            'products/edit.html',
            search_form=self.product_search_form,
            products=products,
            top_tags=top_tags,
            in_edit=True)

    def post(self):
        pass
        if self.product_search_form.is_submitted():
            form = self.product_search_form
            choice = form.search_type.data
            search_text = form.data['search_text']

            if choice == 'tag':
                tag_names = search_text
                return self.get(
                    lambda seller_id: get_tag_search_fn_min_price(tag_names, seller_id))
            elif choice == 'name':
                product_name = search_text
                return self.get(
                    lambda seller_id: get_name_search_fn_min_price(product_name, seller_id))


class DeleteProduct(MethodView):
    decorators = [lambda f: require_levels(f, ('seller',))]

    def post(self):
        if 'product_id' in request.form:
            seller_product.delete(
                request.form['product_id'], auth.current_user())
        return redirect(url_for('products.editor'))


class PriceChangeTemplate(MethodView):
    decorators = [lambda f: require_levels(f, ('seller',))]

    def get(self):
        s = ''
        for product in seller_product.all_from(auth.current_user()):
            s += '{},{}\n'.format(product['id'], product['price'] / 100)
        stream = io.BytesIO()
        stream.write(s.encode())
        stream.seek(0)
        return send_file(stream, as_attachment=True,
                         attachment_filename='price_change.csv',
                         mimetype='text/csv',
                         cache_timeout=0)


class ProductView(MethodView):
    ''' Manages product view (tag suggestions, price reports and history) '''

    def __init__(self):
        self.seller_search_form = SearchForm()
        self.add_tag_form = forms.AddProductTagForm()
        self.price_change_form = forms.PriceChangeForm()

    def get(self, product_id, get_sellers=product.all_sellers):
        product_data = {
            'product_id': product_id,
            'product_name': product.product_name(product_id),
            'top_tags': tags.get_top_tags_product(product_id)
        }

        product_sellers = get_sellers(product_id)

        def get_tags_suggested():
            nonlocal product_id
            id = session.get('id', default=None)
            if id is None:
                return []

            return tags.get_tags_suggested(id, product_id)

        tags_suggested = get_tags_suggested()


        def to_id_name_pair(row):
            return (row['tag_id'], row['tag_name'])

        if session.get('id', None) is not None:
            self.add_tag_form.tag_select.choices = [
                *map(to_id_name_pair, tags.get_unused_tags(
                    session['id'], product_id))]

        if len(tags_suggested) == 5 or \
                len(self.add_tag_form.tag_select.choices) == 0:
            self.add_tag_form.submit.render_kw = \
                dict({'disabled': 'True'})

        return render_template('products/view.html',
                               tags_suggested=tags_suggested,
                               add_tag_form=self.add_tag_form,
                               search_form=self.seller_search_form,
                               price_change_form=self.price_change_form,
                               product_data=product_data, sellers=product_sellers)

    def post(self, product_id):
        submit_text = request.form.get('submit')
        if self.seller_search_form.validate_on_submit():
            form = self.seller_search_form
            seller_name = form.data['search_text'].strip()

            def get_seller_fn(product_id): return seller_product.get_product_seller_name(
                seller_name, product_id)

            return self.get(product_id, get_sellers=get_seller_fn)

        if self.add_tag_form.validate_on_submit():
            tags.add_tag_suggestion(
                user_id=session['id'],
                product_id=product_id,
                tag_id=self.add_tag_form.data['tag_select'])

        return redirect(url_for('products.view', product_id=product_id))

    def delete(self, product_id):
        ''' Removes a tag suggestion made by a user '''

        req_json = request.json
        print(req_json)

        if req_json['opr'] == 'rm_suggested_tag':
            tag_suggestion_id = req_json['tag_suggestion_id']
            tags.remove_tag_suggestion(tag_suggestion_id)

        return make_response({'status': 202})

import sys

class ReportPrice(MethodView):
    decorators = [lambda f: require_levels(f, ('user',))]

    def get(self):
        form = forms.ReportPriceForm()
        product_id = request.args['product_id']
        seller_id = request.args['seller_id']
        return render_template('products/report_price.html',
                               product_id=product_id, seller_id=seller_id, form=form)

    def post(self):
        form = forms.ReportPriceForm()

        product_id = form.product_id.data
        seller_id = form.seller_id.data

        if form.validate():
            user_id = auth.current_user()
            proof = form.proof.data.read()
            new_price = int(form.new_price.data * 100)
            price_changes.add(user_id, product_id, seller_id, proof, new_price)

            return redirect(url_for('products.view', product_id=product_id))

        if product_id and seller_id:
            return render_template('products/report_price.html', form=form,
                                   product_id=product_id, seller_id=seller_id)
        return redirect(url_for('index'))


class PriceChangeView(MethodView):
    decorators = [lambda f: require_levels(f, ('seller',))]

    def get(self):
        form = forms.PriceChangeForm()
        return render_template('products/price_change.html', form=form)

    def post(self):
        form = forms.PriceChangeForm()

        seller_id = session['id']

        if form.validate():
            try:
                raw = form.file.data
                reader = io.TextIOWrapper(raw)
                data = csv.reader(reader, delimiter=',')
                for row in data:
                    seller_product.change_price(
                        seller_id, int(row[0]), int(float(row[1]) * 100))
            except:
                # TODO: This should probably inform the user about the error
                #       and rollback price changes
                return redirect(url_for('products.editor'))

        return redirect(url_for('products.editor'))


class PriceHistory(MethodView):
    def get(self):
        product_id = request.args['product_id']
        seller_id = request.args['seller_id']
        history = seller_product.get_price_history(seller_id, product_id)
        data = {'dates': [], 'prices': []}
        for row in history:
            data['dates'].append(row['date_changed'])
            data['prices'].append(row['price'] / 100)
        return jsonify(data)


product_view = ProductView.as_view('view')
product_editor_view = ProductsEditorView.as_view('editor')

blueprint.add_url_rule('/', view_func=ProductsIndex.as_view('index'))
blueprint.add_url_rule('/new', view_func=NewProductView.as_view('new'))
blueprint.add_url_rule('/view/<int:product_id>', view_func=product_view)
blueprint.add_url_rule('/edit/', view_func=product_editor_view)
blueprint.add_url_rule(
    '/delete_product/', view_func=DeleteProduct.as_view('delete_product'), methods=('POST',))
blueprint.add_url_rule(
    '/report_price', view_func=ReportPrice.as_view('report_price'), methods=('GET', 'POST'))
blueprint.add_url_rule(
    '/price_change', view_func=PriceChangeView.as_view('price_change'), methods=('GET', 'POST'))
blueprint.add_url_rule(
    '/price_history', view_func=PriceHistory.as_view('price_history'), methods=('GET', 'POST'))
blueprint.add_url_rule(
    '/price_change_template', view_func=PriceChangeTemplate.as_view('price_change_template'), methods=('GET',))
