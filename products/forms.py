from flask import request

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired

from wtforms import StringField, IntegerField, DecimalField, SelectField, BooleanField, SubmitField
from wtforms.validators import DataRequired, NumberRange, StopValidation

from ..database import seller_product


class UpdateProductPriceForm(FlaskForm):
    file = FileField()
    submit = SubmitField('Submit')

    def validate(self):
        return True


class NewProductForm(FlaskForm):
    name = StringField('Naziv')
    use_existing = BooleanField('Koristi postojeći')
    existing_product = SelectField(
        'Postojeći proizvodi', choices=[], coerce=int)
    price = DecimalField('Cijena',
                         validators=[DataRequired('Potrebno je unijeti ispravnu cijenu'),
                                     NumberRange(min=0, message='Potrebno je unijeti pozitivnu vrijednost.')])

    def validate(self):
        if not super().validate():
            return False

        if self.use_existing.data:
            if self.existing_product.data == -1:
                self.existing_product.errors = (
                    'Potrebno je odabrati jedan od postojećih proizvoda.',)
                return False
            elif seller_product.get(auth.current_user(), self.existing_product.data) is not None:
                self.existing_product.errors = (
                    'Trgovac već ima taj proizvod dodan.',)
                return False
        elif len(self.name.data.strip()) == 0:
            self.name.errors = ('Potrebno je unijeti ispravan naziv.',)
            return False

        return True


class AddProductTagForm(FlaskForm):
    tag_select = SelectField(
        '', choices=[], coerce=int)

    submit = SubmitField('Dodaj Oznaku')

    def validate(self):
        return request.form.get('submit') == 'Dodaj Oznaku'


class ReportPriceForm(FlaskForm):
    proof = FileField(validators=[FileRequired(), DataRequired(
        'Potreban je dokaz krivo navedene cijene.')])
    product_id = IntegerField(validators=[DataRequired()])
    seller_id = IntegerField(validators=[DataRequired()])
    new_price = DecimalField(validators=[DataRequired(), NumberRange(
        min=0, message='Cijena mora biti veća od 0.')])

    def validate(self):
        if not super().validate():
            return False

        new_price = int(self.new_price.data * 100)
        p = seller_product.get(self.seller_id.data, self.product_id.data)
        if p['price'] == new_price:
            self.new_price.errors = (
                'Potrebno je unijeti cijenu različitu od sadašnje.',)
            return False

        return True


class PriceChangeForm(FlaskForm):
    file = FileField(validators=[FileRequired(),
                                 DataRequired('Potrebno priložiti dokument')])
