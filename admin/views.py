from flask import Blueprint, Response, redirect, render_template, request, session, url_for, make_response
from flask.views import MethodView

from ..database import account, seller, price_changes, tags, seller_product

from ..auth import require_levels

blueprint = Blueprint('admin', __name__, url_prefix='/admin')


class AdminView(MethodView):
    decorators = [lambda f: require_levels(f, ('admin',))]


class PriceChangeManager(AdminView):
    def get(self):
        return render_template('admin/price_change_manager.html', suggestions=price_changes.all())


class TagManager(AdminView):
    def get(self):
        return render_template('admin/tag_manager.html')


class BanAccount(AdminView):
    def post(self):
        account.ban(request.form['id'])
        return redirect(url_for('admin.account_manager'))


class UnbanAccount(AdminView):
    def post(self):
        account.unban(request.form['id'])
        return redirect(url_for('admin.account_manager'))


class PromoteAccount(AdminView):
    def post(self):
        account.promote(request.form['id'])
        return redirect(url_for('admin.account_manager'))


class DemoteAccount(AdminView):
    def post(self):
        account.demote(request.form['id'])
        return redirect(url_for('admin.account_manager'))


class AccountManager(AdminView):
    def get(self):
        return render_template('admin/account_manager.html', accounts=account.all())


class AcceptSuggestion(AdminView):
    def post(self):
        pcid = request.form['id']
        price_changes.set_status(pcid, True, session['id'])
        price_change = price_changes.one(pcid)
        seller_id = price_change['seller_id']
        product_id = price_change['product_id']
        price = price_change['new_price']
        seller.increase_wrong_prices(seller_id)
        seller_product.change_price(seller_id, product_id, price, pcid)
        return redirect(url_for('admin.price_change_manager'))


class DenySuggestion(AdminView):
    def post(self):
        price_changes.set_status(request.form['id'], False, session['id'])
        return redirect(url_for('admin.price_change_manager'))


class ViewSuggestion(AdminView):
    def get(self, id):
        suggestion = price_changes.one(id)
        return render_template('admin/price_change.html', suggestion=suggestion)


class ViewProof(AdminView):
    def get(self, id):
        proof = price_changes.get_proof(id)
        resp = make_response(proof)
        resp.headers.set('Content-Type', 'image')
        return resp


class ViewManageTags(AdminView):
    def get(self):
        return render_template(
            'admin/tag_manager.html',
            tags_count=tags.get_all_tags_count())


class ViewManageTagsApi(AdminView):
    """ REST Api for frontend tag management """

    def post(self):
        """ Adds a new tag to a database.
            Reports an error in case of one. """

        tag_name = request.args.get('tag_name')

        # Implement error handling logic
        if tags.is_tag_name_used(tag_name):
            return make_response("Tag already exists", 409)

        tags.new(tag_name)

        return Response(status=202)


blueprint.add_url_rule(
    '/manage_tags_api', view_func=ViewManageTagsApi.as_view('tag_manager_api'), methods=['GET', 'POST', 'DELETE'])
blueprint.add_url_rule(
    '/manage_tags', view_func=ViewManageTags.as_view('tag_manager'), methods=['GET', 'POST'])
blueprint.add_url_rule(
    '/accounts', view_func=AccountManager.as_view('account_manager'), methods=['GET'])
blueprint.add_url_rule(
    '/price_changes', view_func=PriceChangeManager.as_view('price_change_manager'), methods=['GET'])
blueprint.add_url_rule(
    '/promote_account', view_func=PromoteAccount.as_view('promote_account'), methods=['POST']
)
blueprint.add_url_rule(
    '/demote_account', view_func=DemoteAccount.as_view('demote_account'), methods=['POST']
)
blueprint.add_url_rule(
    '/ban_account', view_func=BanAccount.as_view('ban_account'), methods=['POST']
)
blueprint.add_url_rule(
    '/unban_account', view_func=UnbanAccount.as_view('unban_account'), methods=['POST']
)
blueprint.add_url_rule(
    '/accept_suggestion', view_func=AcceptSuggestion.as_view('accept_suggestion'), methods=['POST']
)
blueprint.add_url_rule(
    '/deny_suggestion', view_func=DenySuggestion.as_view('deny_suggestion'), methods=['POST']
)
blueprint.add_url_rule(
    '/suggestion/<int:id>', view_func=ViewSuggestion.as_view('view_suggestion'), methods=['GET']
)
blueprint.add_url_rule(
    '/proof/<int:id>', view_func=ViewProof.as_view('view_proof'), methods=['GET']
)
