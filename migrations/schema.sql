-- noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE accounts (
  id INTEGER PRIMARY KEY NOT NULL,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  banned BOOLEAN DEFAULT FALSE,
  type TEXT NOT NULL CHECK (type IN ('admin', 'user', 'seller'))
);

CREATE TABLE sellers (
  id INTEGER PRIMARY KEY NOT NULL,
  oib TEXT UNIQUE NOT NULL,
  name TEXT NOT NULL,
  num_wrong_prices INTEGER DEFAULT 0,

  FOREIGN KEY (id) REFERENCES accounts(id)
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL,
  surname TEXT NOT NULL,
  email TEXT UNIQUE NOT NULL,

  FOREIGN KEY (id) REFERENCES accounts(id)
);

CREATE TABLE user_privacy (
    user_id INTEGER PRIMARY KEY NOT NULL,
    name BOOLEAN NOT NULL DEFAULT TRUE,
    surname BOOLEAN NOT NULL DEFAULT TRUE,
    email BOOLEAN NOT NULL DEFAULT TRUE,

    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE products (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL,
  date_added TEXT DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE seller_products (
  product_id INTEGER NOT NULL,
  seller_id INTEGER NOT NULL,
  price INTEGER NOT NULL,

  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (seller_id) REFERENCES sellers(id),
  PRIMARY KEY (product_id, seller_id)
);

CREATE TRIGGER delete_product
    AFTER DELETE ON seller_products
        WHEN old.product_id NOT IN (
            SELECT product_id
            FROM seller_products
        )
BEGIN
    DELETE FROM products
    WHERE id = old.product_id;
END;

CREATE TABLE price_history (
  id INTEGER PRIMARY KEY NOT NULL,
  product_id INTEGER NOT NULL,
  seller_id INTEGER NOT NULL,
  date_changed TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  price INTEGER NOT NULL,
  price_change_id INTEGER,

  FOREIGN KEY (price_change_id) REFERENCES price_changes(id)
);

CREATE TABLE tags (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE tag_suggestions (
  id INTEGER PRIMARY KEY NOT NULL,
  user_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  tag_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (tag_id) REFERENCES tags(id)
);

CREATE TABLE price_changes (
  id INTEGER PRIMARY KEY NOT NULL,
  user_id INTEGER NOT NULL,
  seller_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  proof BLOB NOT NULL,
  new_price INTEGER NOT NULL,
  approved BOOLEAN,
  completed_by INTEGER,
  date_submitted TEXT DEFAULT CURRENT_TIMESTAMP,
  date_completed TEXT,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (seller_id) REFERENCES sellers(id),
  FOREIGN KEY (product_id) REFERENCES products(id)
  FOREIGN KEY (completed_by) REFERENCES accounts(id)
);

CREATE TABLE comments (
  id INTEGER PRIMARY KEY NOT NULL,
  seller_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  comment TEXT NOT NULL,

  FOREIGN KEY (seller_id) REFERENCES sellers(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);