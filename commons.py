""" Collection of Classses and functions
    shared ascorss  submodules
"""

import re

from flask import render_template, redirect, url_for, request
from flask.views import View, MethodView

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField

from .database import product, seller_product

def check_tag_format(tag_str):
    return re.match(r"^#([a-z]|[A-Z]|[0-9])*$", tag_str) is not None


class NoneForm(FlaskForm):
    """ Empty WTForm class
        used for marking none specified forms """

    def validate(self):
        return False


class RenderTemplateView(View):
    """ View dispatch for render_template """

    def __init__(self, template_uri='/', **kwargs):
        self.template_uri = template_uri
        self.kwargs = kwargs

    def dispatch_request(self):
        return render_template(self.template_uri, **self.kwargs)


class RedirectViewLogic(View):
    """ Redirects a request to another endopint
        after executing logic """

    def __init__(self, redirect_endpoint):
        self.redirect_endpoint = redirect_endpoint

    def view_pre_logic(self):
        """ Abstract method.
            Logic preceding a redirect to an endpoint """

        raise NotImplementedError

    def dispatch_request(self):
        self.view_pre_logic()
        return redirect(url_for(self.redirect_endpoint))


class SearchForm(FlaskForm):
    search_text = StringField('Search')
    search_type = SelectField('Pretraži po...', choices=[])

    submit = SubmitField('Traži')

    def validate(self):
        return request.form.get('submit') == 'Traži'

def create_product_search_form(choices=[
    ('name', 'po imenu'), ('tag', 'po oznaci')]):
    product_search_from = SearchForm()
    product_search_from.search_type.choices = choices

    return product_search_from

def get_tag_search_fn_min_price(tag_names, seller_id=None):
    if seller_id is None:
        return lambda : product.all_min_price_tags(tag_names)
    return seller_product.all_from_tags_like(seller_id, tag_names)


def get_name_search_fn_min_price(product_name, seller_id=None):
    if seller_id is None:
        return lambda : product.get_by_name_min_price(product_name)
    return seller_product.all_from_name_like(seller_id, product_name)

class PostGetFormView(MethodView):
    """ Wrapper class for GET template rendering
        and POST logic template delivery
    """

    def __init__(self, form, template_uri, **kwargs):
        """ Default parameter constructor.
            All arguments required.

            Args:
                form: form used in template construction
                template_uri: points to the rendered template
                kwargs: key word arguments forwarded to renver view
        """
        self.form = form
        self.template_uri = template_uri
        self.kwargs = kwargs

    def view_post_logic(self):
        """ Abstract method.
            Logic followed after successful form validation """

        raise NotImplementedError

    def post(self):
        if self.form.validate_on_submit():
            return self.view_post_logic()
        else:
            return self.get()

    def get(self):
        return render_template(self.template_uri, form=self.form, **self.kwargs)
