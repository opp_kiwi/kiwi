from . import commons
import os
import click
from . import database
from . import auth
from . import gen_dummy, gen_dummy_real

from flask import Flask, render_template, redirect, url_for, session, request, current_app
from flask.views import MethodView
from flask.cli import with_appcontext

from flask_wtf.csrf import CSRFProtect

csrf = CSRFProtect()


@click.command('init-db')
@with_appcontext
def init_db_command():
    ''' Creates new database.
        Previous db instance is deleted. '''
    database.init_db()

    auth.create_account({
        'username': current_app.config['ADMIN_USERNAME'],
        'password': current_app.config['ADMIN_PASS'],
        'name': current_app.config['ADMIN_NAME'],
        'surname': '',
        'email': ''
    }, 'admin')

    click.echo('Initialized the database.')


@click.command('dummy-db')
@with_appcontext
def init_dummy_db_command():
    ''' Fills database with dummy data entries.
            Assumes database has been initialized '''
    gen_dummy.generate()
    click.echo('Initialized the database with dummy data')


@click.command('dummy-real-db')
@with_appcontext
def init_real_dummy_db_command():
    gen_dummy_real.generate()
    click.echo('Initialized the database with realistic dummy data')


def create_app(test_conf=None):
    # Initialize a flaks app
    app = Flask(__name__, instance_relative_config=True)
    csrf.init_app(app)

    # Basic app configuration
    app.config.from_mapping(
        SECRET_KEY='DEV',
        ADMIN_USERNAME='admin',
        ADMIN_PASS='admin%pass',
        ADMIN_NAME='Glavni administrator',
        DATABASE=os.path.join(app.instance_path, 'db.sql')
    )

    # TODO: Configure logger to a file in an instance folder

    # Check for instance folder
    if not os.path.exists(app.instance_path):
        try:
            os.makedirs(app.instance_path)
        except OSError as e:
            app.logger.error(f"Boot error: {e.strerror}")

    # Check for test config
    if test_conf is not None:
        app.config.from_mapping(test_conf)
    else:  # Load regular config if found
        app.config.from_pyfile('config.py', silent=True)
    # TODO: Write server configuration in an instance

    app.teardown_appcontext(database.close_db)

    app.cli.add_command(init_dummy_db_command)
    app.cli.add_command(init_real_dummy_db_command)
    app.cli.add_command(init_db_command)

    from .database import account

    @app.before_request
    def check_if_banned():
        if str(request.url_rule) != url_for('auth.logout'):
            if auth.is_logged_in() and account.is_banned(session['id']):
                return render_template('banned.html')

    from .database import product, tags

    class IndexView(MethodView):
        def __init__(self):
            self.product_search_form = commons.create_product_search_form()

        def get(self, get_products_fn=product.all_min_price):
            products = get_products_fn()
            top_tags = tags.get_top_tags_products(products)

            return render_template(
                'index.html',
                search_form=self.product_search_form,
                products=products,
                top_tags=top_tags)

        def post(self):
            if self.product_search_form.is_submitted():
                form = self.product_search_form
                choice = form.search_type.data
                search_text = form.data['search_text']

                if choice == 'tag':
                    tag_names = search_text
                    return self.get(
                        commons.get_tag_search_fn_min_price(tag_names))
                elif choice == 'name':
                    product_name = search_text
                    return self.get(
                        commons.get_name_search_fn_min_price(product_name))

            return redirect(url_for('index'))

    app.add_url_rule('/', view_func=IndexView.as_view('index'))

    # Blueprint registration
    from . import auth
    app.register_blueprint(auth.views.blueprint)

    from . import products
    app.register_blueprint(products.views.blueprint)

    from . import accounts
    app.register_blueprint(accounts.views.blueprint)

    from . import admin
    app.register_blueprint(admin.views.blueprint)

    return app


wsgi_app = create_app()

if __name__ == '__main__':
    wsgi_app.run()
