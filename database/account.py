from . import use_cursor


@use_cursor
def exists(c, id):
    c.execute('SELECT * FROM accounts WHERE id=?', (id,))
    return c.fetchone() is not None


@use_cursor
def get_comment(c, seller_id):
    c.execute('SELECT comment FROM comments WHERE seller_id=?', (seller_id,))
    row = c.fetchone()
    if row:
        return row['comment']
    return None


@use_cursor
def username(c, id):
    c.execute('SELECT username FROM accounts WHERE id=?', (id,))
    row = c.fetchone()
    if row:
        return row['username']
    return None


@use_cursor
def type(c, id):
    c.execute('SELECT type FROM accounts WHERE id=?', (id,))
    return c.fetchone()['type']


@use_cursor
def id(c, username):
    c.execute('SELECT id FROM accounts WHERE username=?', (username,))
    row = c.fetchone()
    if row:
        return row['id']
    return None


@use_cursor
def all_with_info(c):
    c.execute('SELECT *, (SELECT ) FROM accounts')


@use_cursor
def ban(c, id):
    c.execute('UPDATE accounts SET banned=TRUE WHERE id=?', (id,))


@use_cursor
def unban(c, id):
    c.execute('UPDATE accounts SET banned=FALSE WHERE id=?', (id,))


@use_cursor
def promote(c, id):
    c.execute('UPDATE accounts SET type="admin" WHERE id=?', (id,))


@use_cursor
def demote(c, id):
    c.execute('UPDATE accounts SET type="user" WHERE id=?', (id,))


@use_cursor
def username_exists(c, username):
    c.execute('SELECT * FROM accounts WHERE username=?', (username,))
    return c.fetchone() is not None


@use_cursor
def password(c, username):
    c.execute('SELECT password FROM accounts WHERE username=?', (username,))
    row = c.fetchone()
    if not row:
        return False
    return row['password']


@use_cursor
def add_new(c, d):
    c.execute('INSERT INTO accounts (username, password, type) VALUES (?,?,?)',
              (d['username'], d['password'], d['type']))
    return c.lastrowid


@use_cursor
def all(c):
    c.execute('SELECT * FROM accounts')
    rows = c.fetchall()
    return rows


@use_cursor
def is_banned(c, id):
    c.execute('SELECT banned FROM accounts WHERE id=?', (id,))
    return c.fetchone()['banned']
