from . import use_cursor


@use_cursor
def type(c, id):
    c.execute('SELECT type FROM users WHERE id=?', (id,))
    row = c.fetchone()
    if row:
        return row['type']
    return None


@use_cursor
def email_exists(c, email):
    c.execute('SELECT * FROM users WHERE email=?', (email,))
    return c.fetchone() is not None


@use_cursor
def add_new(c, d):
    c.execute('INSERT INTO users (id, name, surname, email) VALUES (?,?,?,?)',
              (d['id'], d['name'], d['surname'], d['email']))


@use_cursor
def get(c, id):
    c.execute('SELECT * FROM users WHERE id=?', (id,))
    return c.fetchone()


@use_cursor
def get_privacy_settings(c, id):
    c.execute('SELECT * FROM user_privacy WHERE user_id=?', (id,))
    return c.fetchone()


@use_cursor
def add_privacy_info(c, id):
    c.execute('INSERT INTO user_privacy (user_id) VALUES (?)', (id,))


@use_cursor
def update_privacy_info(c, id, name, surname, email):
    c.execute('UPDATE user_privacy SET name=?, surname=?, email=? WHERE user_id=?',
              (name, surname, email, id))
