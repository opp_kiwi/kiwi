from . import use_cursor


@use_cursor
def all(c):
    c.execute('SELECT * FROM products')
    return c.fetchall()


@use_cursor
def product_name(c, product_id):
    c.execute('SELECT name FROM products WHERE id=?', (product_id,))

    row = c.fetchone()
    if row is not None:
        return row[0]

    # TODO: Develop error handling logic
    return None


@use_cursor
def all_sellers(c, product_id):
    c.execute(
        '''SELECT 
                p.id AS product_id, 
                p.name AS product_name,
                sp.seller_id AS seller_id,
                s.name AS seller_name, 
                sp.price AS price
            FROM seller_products sp INNER JOIN sellers s
                ON sp.seller_id = s.id
                    INNER JOIN products p ON sp.product_id = p.id
             WHERE p.id = ?
            ORDER BY sp.price;''', (product_id,))

    return c.fetchall()


@use_cursor
def all_min_price(c):
    c.execute(
        '''SELECT 
            p.id AS product_id, 
            p.name AS product_name,
            min(sp.price) AS price
        FROM products p INNER JOIN seller_products sp
            ON p.id = sp.product_id
        GROUP BY id, name;''')

    return c.fetchall()


@use_cursor
def all_min_price_tags(c, tag_names):
    ''' Searches products by tags.
        Matches minimum price.

        Product will be returned if it's matched at leas with one tag. '''

    # Strip leading '#'
    tag_names = list(map(lambda tag_name: '%' + tag_name[1:] + '%',
                         tag_names.split()))

    def generate_wheres():
        nonlocal tag_names
        base = 't.name LIKE (?)'
        clause = 't.name LIKE (?)'
        for tag_name in range(len(tag_names) - 1):
            clause += ' OR ' + base

        return clause

    print(tag_names)

    c.execute(
        '''SELECT 
            p.id AS product_id, 
            p.name AS product_name,
            min(sp.price) AS price,
            sp.seller_id AS seller_id
        FROM products p INNER JOIN seller_products sp
            ON p.id = sp.product_id
                INNER JOIN tag_suggestions ts
                    ON p.id == ts.product_id
                        INNER JOIN tags t ON t.id = ts.tag_id
            WHERE ''' + generate_wheres() + '''
        GROUP BY p.id, p.name;''', tuple(tag_names))


    return c.fetchall()


@use_cursor
def get_by_name_min_price(c, product_name):
    c.execute('''
        SELECT 
            p.id AS product_id, 
            p.name AS product_name,
            min(sp.price) AS price,
            sp.seller_id AS seller_id
        FROM products p INNER JOIN seller_products sp
            ON p.id = sp.product_id
        WHERE p.name LIKE (?)
        GROUP BY id, name''', ('%' + product_name + '%', ))

    return c.fetchall()


@use_cursor
def new(c, name):
    c.execute('INSERT INTO products (name) VALUES (?)', (name,))
    return c.lastrowid
