''' DB API for storing product tags
    NOTE: tags are stored without following '#' '''

from . import use_cursor


@use_cursor
def get_all_tags(c):
    ''' Fetches all tags in s database.
        Regardless of their use count '''

    c.execute('''
        SELECT
            id AS tag_id,
            name AS tag_name
            FROM tags;''')

    return c.fetchall()


@use_cursor
def get_all_tags_count(c):
    ''' How many times has each tag been associated with a product '''

    c.execute('''
        SELECT 
            t.id AS tag_id,
            t.name AS tag_name, 
            COUNT(ts.id) AS uses
        FROM tags t LEFT JOIN tag_suggestions ts
            ON t.id = ts.tag_id
        GROUP BY t.id, t.name;
    ''')

    return c.fetchall()


@use_cursor
def get_tag_usages(c, tag_id):
    ''' Returns tag suggestions associated with a tag_id '''

    c.execute('''
        SELECT
            ts.id AS suggestion_id,
            *
        FROM tag_suggestions ts
        WHERE ts.tag_id = (?);
    ''', (tag_id))

    return c.fetchall()


@use_cursor
def new(c, tag_name):
    ''' Inserts a tage into a database '''

    c.execute('''
        INSERT INTO tags (name) VALUES (?);''', (tag_name,))

    return c.lastrowid

@use_cursor
def is_tag_name_used(c, tag_name):
    ''' Checks if tag name is found in database '''

    c.execute('''
        SELECT 
            COUNT(id) as count
        FROM tags
        WHERE name = (?);''', (tag_name,))

    return c.fetchone()['count'] != 0


@use_cursor
def get_top_tags_product(c, product_id):
    ''' Returns top 5 tag suggestions for a given product '''

    c.execute('''
        SELECT
            t.name AS tag_name,
            COUNT(ts.id)
        FROM tags t JOIN tag_suggestions ts
            ON t.id = ts.tag_id AND ts.product_id = (?)
        GROUP BY t.name
        ORDER BY COUNT(ts.id) DESC
        LIMIT 5;''', (product_id,))

    return c.fetchall()


def get_top_tags_products(products):
    if products is None or len(products) == 0:
        return []

    return {
        row['product_id']: get_top_tags_product(row['product_id']) \
        for row in products
    }


@use_cursor
def get_tags_suggested(c, user_id, product_id):
    ''' Fetches all tags suggested by user for a product '''

    c.execute('''
        SELECT 
            t.id AS tag_id,
            t.name AS tag_name,
            ts.id AS tag_suggestion_id
        FROM tags t JOIN tag_suggestions ts
            ON t.id = ts.tag_id
        WHERE ts.user_id = (?)
                AND ts.product_id = (?);
    ''', (user_id, product_id,))

    return c.fetchall()


@use_cursor
def add_tag_suggestion(c, user_id, product_id, tag_id):
    ''' Adds a new user tag suggestion for a given product '''

    c.execute('''
        INSERT INTO tag_suggestions (user_id, product_id, tag_id)
            VALUES ((?), (?), (?));''', (user_id, product_id, tag_id,))

    return c.lastrowid


@use_cursor
def get_unused_tags(c, user_id, product_id):
    ''' Fetch all unused tags by a user
    for a specified product '''

    c.execute('''
            SELECT
                t.id AS tag_id,
                t.name AS tag_name
                FROM tags t
                WHERE t.id NOT IN (
                    SELECT ts.tag_id
                    FROM tag_suggestions ts
                    WHERE ts.user_id = (?) and ts.product_id = (?)    
                );
                 
        ''', (user_id, product_id, ))

    return c.fetchall()

@use_cursor
def remove_tag_suggestion(c, tag_suggestion_id):
    ''' Removes a tag suggestion from database '''

    c.execute('''
        DELETE FROM tag_suggestions
        WHERE id = (?)''', (tag_suggestion_id, ))

    return c.lastrowid