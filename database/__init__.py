import os

import sqlite3

from flask import current_app, g



def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        db.row_factory = sqlite3.Row

        g._database = db
    return db


def close_db(e=None):
    db = g.pop('_database', None)

    if db is not None:
        db.close()

# Usage


def use_db(f):
    def wrapper(*args, **kwargs):
        db = get_db()
        result = f(db, *args, **kwargs)
        db.commit()
        return result
    return wrapper


def use_cursor(f):
    @use_db
    def wrapper(db, *args, **kwargs):
        c = db.cursor()
        return f(c, *args, **kwargs)

    return wrapper


# CLI and app integration
def init_db():
    if os.path.exists(current_app.config['DATABASE']):
        os.remove(current_app.config['DATABASE'])

    db = get_db()

    with current_app.open_resource('migrations/schema.sql') as f:
        db.executescript(f.read().decode('utf8'))
