from . import use_cursor


@use_cursor
def get_comments(c, seller_id):
    c.execute('''SELECT users.name, users.surname, comments.comment FROM comments LEFT JOIN users
                 ON comments.user_id = users.id WHERE seller_id=?''', (seller_id,))
    return c.fetchall()


@use_cursor
def put_comment(c, seller_id, user_id, comment):
    c.execute('SELECT comment FROM comments WHERE seller_id=? AND user_id=?',
              (seller_id, user_id))
    row = c.fetchone()
    if row:
        c.execute('UPDATE comments SET comment=? WHERE seller_id=? AND user_id=?',
                  (comment, seller_id, user_id))
    else:
        c.execute('INSERT INTO comments (seller_id, user_id, comment) VALUES (?,?,?)',
                  (seller_id, user_id, comment))
