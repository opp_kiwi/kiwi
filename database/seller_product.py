from . import use_cursor


@use_cursor
def get(c, seller_id, product_id):
    c.execute('SELECT * FROM seller_products WHERE seller_id=? AND product_id=?',
              (seller_id, product_id))
    return c.fetchone()


@use_cursor
def all_from(c, seller_id):
    c.execute(
        '''SELECT 
                name AS product_name, * FROM seller_products
           JOIN products ON seller_products.product_id = products.id
           WHERE seller_id=?''', (seller_id,))
    return c.fetchall()


@use_cursor
def all_from_name_like(c, seller_id, product_name):
    c.execute(
        '''SELECT
                name AS product_name, * FROM seller_products
            JOIN products ON seller_products.product_id = products.id
            WHERE seller_id=? AND name LIKE (?);''', (seller_id, '%' + product_name + '%'))

    return c.fetchall()

@use_cursor
def all_from_tags_like(c, seller_id, tag_names):
    ''' Searches products by tags.
        Matches minimum price.

        Product will be returned if it's matched at leas with one tag. '''

    # Strip leading '#'
    tag_names = list(map(lambda tag_name: '%' + tag_name[1:] + '%',
                         tag_names.split()))

    def generate_wheres():
        nonlocal tag_names
        base = 't.name LIKE (?)'
        clause = 't.name LIKE (?)'
        for tag_name in range(len(tag_names) - 1):
            clause += ' OR ' + base

        return clause

    print(tag_names)

    c.execute(
        '''SELECT 
            p.id AS product_id, 
            p.name AS product_name,
            min(sp.price) AS price,
            sp.seller_id AS seller_id
        FROM products p INNER JOIN seller_products sp
            ON p.id = sp.product_id AND sp.seller_id = (?)
                INNER JOIN tag_suggestions ts
                    ON p.id == ts.product_id
                        INNER JOIN tags t ON t.id = ts.tag_id
            WHERE ''' + generate_wheres() + '''
        GROUP BY p.id, p.name;''', (seller_id, ) + tuple(tag_names))


    return c.fetchall()

@use_cursor
def change_price(c, seller_id, product_id, price, price_change_id=None):
    c.execute('''UPDATE seller_products SET price=? WHERE seller_id=? AND product_id=?''',
              (price, seller_id, product_id))
    c.execute('''INSERT INTO price_history
                 (seller_id, product_id, price, price_change_id) VALUES (?,?,?,?)''',
              (seller_id, product_id, price, price_change_id))


# Price history = last 7 days
@use_cursor
def get_price_history(c, seller_id, product_id):
    c.execute('''SELECT * FROM price_history
                 LEFT JOIN products ON price_history.product_id = products.id
                 WHERE seller_id=? AND product_id=?
                 AND date_changed >= datetime('now', '-7 days') ORDER BY date_changed''',
              (seller_id, product_id))
    return c.fetchall()


@use_cursor
def new(c, product_id, seller_id, price):
    c.execute('INSERT INTO seller_products (product_id, seller_id, price) VALUES (?,?,?)',
              (product_id, seller_id, price))
    c.execute('''INSERT INTO price_history
                 (seller_id, product_id, price) VALUES (?,?,?)''',
              (seller_id, product_id, price))
    return c.lastrowid


@use_cursor
def get_product_seller_name(c, seller_name, product_id):
    c.execute('''
        SELECT 
            p.id AS product_id, 
            p.name AS product_name,
            sp.seller_id AS seller_id,
            s.name AS seller_name, 
            sp.price AS price
        FROM products p JOIN seller_products sp
            ON p.id = sp.product_id JOIN sellers s
                ON sp.seller_id = s.id
        WHERE s.name LIKE (?) AND p.id = (?);''', ('%' + seller_name + '%', product_id,))

    return c.fetchall()


@use_cursor
def delete(c, product_id, seller_id):
    c.execute('''
        DELETE FROM seller_products
        WHERE product_id = (?) AND seller_id = (?);
        ''', (product_id, seller_id, ))
