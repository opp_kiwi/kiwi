from . import use_cursor


@use_cursor
def add_new(c, d):
    c.execute('INSERT INTO sellers (id, oib, name) VALUES (?,?,?)',
              (d['id'], d['oib'], d['name']))


@use_cursor
def increase_wrong_prices(c, id):
    c.execute(
        'UPDATE sellers SET num_wrong_prices=num_wrong_prices + 1 WHERE id=?', (id,))


@use_cursor
def get(c, id):
    c.execute('SELECT * FROM sellers WHERE id=?', (id,))
    return c.fetchone()


@use_cursor
def oib_exists(c, oib):
    c.execute('SELECT * FROM sellers WHERE oib=?', (oib,))
    return c.fetchone() is not None
