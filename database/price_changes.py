from . import use_cursor


@use_cursor
def all(c):
    c.execute('''SELECT price_changes.id,
                        price_changes.user_id, users.name AS user_name, users.surname AS user_surname,
                        price_changes.seller_id, sellers.name AS seller_name,
                        price_changes.product_id, products.name AS product_name,
                        price_changes.new_price, price_changes.approved,
                        seller_products.price
                 FROM price_changes
                 LEFT JOIN products, users, sellers, seller_products
				 ON price_changes.product_id = products.id
				 AND price_changes.user_id = users.id
				 AND price_changes.seller_id = sellers.id
                 AND seller_products.product_id = price_changes.product_id
				 AND seller_products.seller_id = price_changes.seller_id''')
    return c.fetchall()


@use_cursor
def one(c, id):
    c.execute('''SELECT price_changes.id,
                        price_changes.user_id, users.name AS user_name, users.surname AS user_surname,
                        price_changes.seller_id, sellers.name AS seller_name,
                        price_changes.product_id, products.name AS product_name,
                        price_changes.completed_by, admin.name AS admin_name, admin.surname AS admin_surname,
                        price_changes.new_price, price_changes.approved,
                        price_changes.date_submitted, price_changes.date_completed,
                        seller_products.price
                 FROM price_changes
                 LEFT JOIN products, users, sellers, seller_products, users AS admin
				 ON price_changes.product_id = products.id
                 AND price_changes.user_id = users.id
                 AND price_changes.seller_id = sellers.id
                 AND price_changes.completed_by = admin.id
                 AND seller_products.product_id = price_changes.product_id
				 AND seller_products.seller_id = price_changes.seller_id
				 WHERE price_changes.id=?''', (id,))
    return c.fetchone()


@use_cursor
def set_status(c, id, value, admin_id):
    c.execute('UPDATE price_changes SET approved=?, completed_by=?, date_completed=CURRENT_TIMESTAMP WHERE id=?',
              (value, admin_id, id))


@use_cursor
def get_proof(c, id):
    c.execute('SELECT proof FROM price_changes WHERE id=?', (id,))
    return c.fetchone()['proof']


@use_cursor
def add(c, user_id, product_id, seller_id, proof, new_price):
    c.execute('''INSERT INTO price_changes (user_id, product_id, seller_id, proof, new_price)
                 VALUES (?,?,?,?,?)''', (user_id, product_id, seller_id, proof, new_price))
