''' Python script for filling a database with dummy data.
    Assumes that the database has already been initialized '''

import random
import string

from werkzeug.security import generate_password_hash

from .database import account, user, seller, product, seller_product, tags

PASSWORD = 'password123'  # Universal password for login, doesn't apply to admin
PASS_HASH = generate_password_hash(PASSWORD)

N_USERS = 143  # Prescribed number of dummy users
N_SELLERS = 7  # Prescribed number of dummy sellers

N_PRODUCTS = 35  # Total number of products in the store
N_PRODUCTS_PER = 22  # Prescribed number of products per store

N_TAGS = 7 * 35

N_USERS_TAGGING_LO = 0  # Minimum number of users tagging a product
N_USERS_TAGGING_HI = 19  # Maximum (excluded) number of users tagging a product

EMAIL_DOMAIN = 'dummy.opp'

users = []
sellers = []

products = []
seller_products = {}
special_products = {}  # One for one, one for all

generated_tags = []
user_tags = {}


def gen_users():
    ''' Generates dummy users, all have the same password '''

    common_data = {
        'password': PASS_HASH,
        'type': 'user'
    }

    base_data = {
        'username': 'dummy_user',

        'name': 'ime',
        'surname': 'prezime',
    }

    def gen_user_data(id):
        nonlocal common_data
        nonlocal base_data

        user_data = dict(common_data)
        user_data.update({key: value + str(id)
                          for key, value in base_data.items()})

        user_data['email'] = user_data['name'] + '.' + user_data['surname']
        user_data['email'] += EMAIL_DOMAIN

        return user_data

    for i in range(N_USERS):
        user_data = gen_user_data(i)
        acc_id = account.add_new(user_data)
        user_data['id'] = acc_id

        user.add_privacy_info(acc_id)

        user.add_new(user_data)
        users.append(user_data)


def gen_sellers():
    ''' Generates dummy sellers, all have the same password '''

    common_data = {
        'password': PASS_HASH,
        'type': 'seller'
    }

    base_data = {
        'username': 'dummy_seller',
        'name': 'dummy_seller'
    }

    def gen_rng_oib():
        digits = string.digits
        return ''.join(random.choice(digits) for _ in range(11))

    def gen_seller_data(id):
        nonlocal common_data
        nonlocal base_data

        seller_data = dict(common_data)
        seller_data.update({'id': id, 'oib': gen_rng_oib()})
        seller_data.update({key: value + str(id)
                            for key, value in base_data.items()})

        return seller_data

    for i in range(N_SELLERS):
        seller_data = gen_seller_data(i)
        acc_id = account.add_new(seller_data)
        seller_data['id'] = acc_id

        seller.add_new(seller_data)
        sellers.append(seller_data)


def gen_products():
    ''' Generates random products '''

    base_name = 'product'
    for i in range(N_PRODUCTS):
        product_name = base_name + str(i)
        product_id = product.new(product_name)

        products.append({
            'id': product_id,
            'name': product_name
        })

    special_products['single_seller'] = random.choice(products)
    products.remove(special_products['single_seller'])

    special_products['all_sellers'] = random.choice(products)
    products.remove(special_products['all_sellers'])

    special_products['10tag_product'] = random.choice(products)
    products.remove(special_products['10tag_product'])


def gen_seller_products():
    ''' Randomly assign products to sellers '''

    for s in sellers:
        rng_products = random.sample(products, N_PRODUCTS_PER)
        for sp in rng_products:
            price = random.randint(0, 1001) * 100
            seller_product_id = seller_product.new(sp['id'], s['id'], price)


def gen_tags():
    ''' Generate random tags '''

    for i in range(N_TAGS):
        tag_name = f"tAg{i}"
        tag_id = tags.new(tag_name)
        generated_tags.append({
            'id': tag_id,
            'name': tag_name
        })


def gen_tag_suggestons():
    ''' Generates tag suggestions.
        Randomly picks a triplet (user_id, product_id, tag_id) '''

    def tag_product(prod, rng_users):
        for rng_usr in random.sample(users, n_users):
            n_tags = random.randint(0, 6)
            for rng_tag in random.sample(generated_tags, n_tags):
                tags.add_tag_suggestion(
                    rng_usr['id'], prod['id'], rng_tag['id'])

    n_products = random.randint(N_PRODUCTS_PER, len(products))
    for prod in random.sample(products, n_products):
        n_users = random.randint(N_USERS_TAGGING_LO, N_USERS_TAGGING_HI)

        tag_product(prod, random.sample(users, n_users))

    # Special requirement
    n_users = 12
    tag_product(special_products['10tag_product'], n_users)


def generate():
    gen_users()
    gen_sellers()

    gen_products()
    gen_seller_products()

    gen_tags()
    gen_tag_suggestons()
