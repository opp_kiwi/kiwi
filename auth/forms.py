from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, RadioField
from wtforms.validators import Email, DataRequired, ValidationError, StopValidation, Length, Regexp

from ..database import account, user, seller
from .. import auth


def at_least(name, n):
    return '{} mora imati najmanje {} znakova.'.format(name, n)


def between(name, n1, n2):
    return '{} mora imati između {} i {} znakova.'.format(name, n1, n2)


class RegisterForm(FlaskForm):
    username = StringField('Korisničko ime',
                           validators=[Length(min=8, max=16, message=between('Korisničko ime', 8, 16))])
    password = PasswordField('Lozinka',
                             validators=[Length(min=8, message=at_least('Lozinka', 8))])
    password_repeat = PasswordField('Ponovljena lozinka',
                                    validators=[DataRequired(message='Potrebno je ponovno unijeti lozinku!')])

    def validate_username(form, field):
        if account.username_exists(field.data):
            raise ValidationError(
                'Korisnik s takvim korisničkim imenom već postoji.')

    def validate(self):
        if not super().validate():
            return False
        print(self.password.data, self.password_repeat.data)
        if self.password.data != self.password_repeat.data:
            self.password_repeat.errors = ('Lozinke se ne podudaraju.',)
            return False
        return True


class RegistrationTypeForm(FlaskForm):
    type = RadioField('Želim se registrirati kao',
                      choices=(('user', 'korisnik'), ('seller', 'trgovina')))


class RegisterSellerForm(RegisterForm):
    name = StringField('Ime trgovine', validators=[
        DataRequired('Potrebno je unijeti ime trgovine.')])
    oib = StringField('OIB', validators=[Regexp(
        r'^[0-9]{11}$', message='Neispravan OIB.')])

    def validate(self):
        if not super().validate():
            return False
        if seller.oib_exists(self.oib.data):
            self.oib.errors = ('Trgovac s ovim OIB-om već postoji.',)
            return False
        return True


class RegisterUserForm(RegisterForm):
    name = StringField('Ime', validators=[
        DataRequired('Potrebno je unijeti ime.')])
    surname = StringField('Prezime', validators=[
        DataRequired('Potrebno je unijeti prezime.')])
    email = StringField('Email', validators=[
        Email('Unesite ispravnu email adresu.')])

    def validate(self):
        if not super().validate():
            return False
        if user.email_exists(self.email.data):
            self.email.errors = ('Korisnik s ovim emailom već postoji.',)
            return False
        return True


class LoginForm(FlaskForm):
    username = StringField('Korisničko ime', validators=[
        DataRequired('Potrebno je unijeti korisničko ime.')])
    password = PasswordField('Lozinka', validators=[
        DataRequired('Potrebno je unijeti lozinku.')])

    def validate(self):
        if not super().validate():
            return False

        username = self.username.data
        password = self.password.data

        if not account.username_exists(username):
            self.username.errors = (
                'Ne postoji korisnik s takvim korisničkim imenom.',)
            return False
        if not auth.check_credentials(username, password):
            self.password.errors = ('Kriva lozinka.',)
            return False

        return True
