from flask import session
from werkzeug.security import check_password_hash, generate_password_hash

from ..database import user, account, seller
from . import views

from flask import render_template


def check_account(f):
    def wrapper(*args, **kwargs):
        if not 'id' in session:
            return redirect(url_for('auth.login'))

        if not account.exists(session['id']):
            logout()
            return redirect(url_for('auth.login'))

        return f(*args, **kwargs)


def require_levels(f, levels):
    def wrapper(*args, **kwargs):
        if not 'id' in session:
            return redirect(url_for('auth.login'))

        if not account.exists(session['id']):
            logout()
            return redirect(url_for('auth.login'))

        t = session['account_type']

        if t not in levels:
            return render_template('unauthorized.html')

        return f(*args, **kwargs)

    return wrapper


def current_user():
    if not is_logged_in():
        return None
    return session['id']


def is_logged_in():
    if not 'id' in session or not account.exists(session['id']):
        return False
    return True


def check_credentials(username, password):
    real_password = account.password(username)
    return check_password_hash(real_password, password)


def create_account(d, type):
    d['password'] = generate_password_hash(d['password'])
    d['type'] = type

    acc_id = account.add_new(d)
    d['id'] = acc_id

    if type == 'seller':
        seller.add_new(d)
    elif type in ('user', 'admin'):
        user.add_new(d)

    if type == 'user':
        user.add_privacy_info(acc_id)

    return acc_id


def login(username):
    account_id = account.id(username)
    session['id'] = account_id
    session['account_type'] = account.type(account_id)
    session['username'] = account.username(account_id)


def logout():
    del session['id']
    del session['account_type']
    del session['username']
