from functools import wraps

from flask import Blueprint, redirect, render_template, request, session, url_for
from flask.views import View, MethodView

from .. database import account
from .. import auth

from .. commons import RedirectViewLogic, PostGetFormView, SearchForm
from . import forms

blueprint = Blueprint('auth', __name__, url_prefix='/auth')

def check_logged_in(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if auth.is_logged_in():
            return redirect(url_for('index'))
        
        return f(*args, **kwargs)

    return wrapper

class LogoutRedirectView(RedirectViewLogic):
    def __init__(self):
        super(LogoutRedirectView, self).__init__('index')

    def view_pre_logic(self):
        auth.logout()

class AuthView(PostGetFormView):
    decorators = [check_logged_in]

    def __init__(self, form, template_uri):
        super(AuthView, self).__init__(form, template_uri)

class LoginView(AuthView):
    def __init__(self):
        super(LoginView, self).__init__(forms.LoginForm(), 'auth/login.html')

    def view_post_logic(self):
        username = self.form.username.data
        auth.login(username)

        return redirect(url_for('index'))

class RegistrationTypeView(AuthView):
    def __init__(self):
        super(RegistrationTypeView, self).__init__(forms.RegistrationTypeForm(), 'auth/register.html')

    def view_post_logic(self):
        if self.form.type.data == 'user':
            return redirect(url_for('auth.register_user'))
        else:
            return redirect(url_for('auth.register_seller'))

class RegisterUserView(AuthView):
    def __init__(self):
        super(RegisterUserView, self).__init__(forms.RegisterUserForm(), 'auth/register_user.html')

    def view_post_logic(self):
        auth.create_account(self.form.data, 'user')
        auth.login(self.form.username.data)

        return redirect(url_for('index'))


class RegisterSellerView(AuthView):
    def __init__(self):
        super(RegisterSellerView, self).__init__(forms.RegisterSellerForm(), 'auth/register_seller.html')

    def view_post_logic(self):
        auth.create_account(self.form.data, 'seller')
        auth.login(self.form.username.data)

        return redirect(url_for('index'))

blueprint.add_url_rule('/login', view_func=LoginView.as_view('login'), methods=['GET', 'POST'])
blueprint.add_url_rule('/register', view_func=RegistrationTypeView.as_view('register'))
blueprint.add_url_rule('/register/user', view_func=RegisterUserView.as_view('register_user'))
blueprint.add_url_rule('/register/seller', view_func=RegisterSellerView.as_view('register_seller'))
blueprint.add_url_rule('/logout', view_func=LogoutRedirectView.as_view('logout'))