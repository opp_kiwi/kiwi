import random
from .database import account, user, seller, product, seller_product, tags
from . import auth

NAMES = ('Dario', 'Matija', 'Viktor', 'Marija', 'Ana', 'Josipa',
         'Petar', 'Vedran', 'Tvrtko', 'Silvio', 'Stella', 'Katarina', 'Hrvoje', 'Karlo', 'Matej')
SELLER_PREFIXES = ('Trgovina', 'Trgovački lanac', 'OPG')
SURNAMES = ('Kević', 'Brečić', 'Brekalo', 'Vugdelija',
            'Kurdija', 'Grabovac', 'Miličević', 'Marinović', 'Horvat', 'Kovačević', 'Knežević', 'Babić')
DOMAINS = ('gmail.com', 'fer.hr', 'yahoo.com')
PRODUCTS = ('Paprika', 'Kobasica', 'Hrenovke', 'Rajčica',
            'Kruh', 'Vrhnje', 'Jogurt', 'Mlijeko', 'Čips', 'Suncokretovo ulje', 'Krastavci', 'Tuna u konzervi', 'Cola', 'Fanta',
            'Schweppes', 'Luk', 'Poriluk', 'Kruška', 'Jabuka', 'Maslinovo ulje', 'Orada', 'Som', 'Tuna', 'Feferoni', 'Skuša', 'Kikiriki')

NUM_TAGS = 20
NUM_USERS = 20
NUM_SELLERS = 5
NUM_PRODUCTS = len(PRODUCTS)
PASSWORD = 'password'

_users = []
_sellers = []
_products = []
_tags = []
_seller_products = []


def _remove_diacritics(s):
    LETTERS = 'CcCcDdSsZz'
    for i, c in enumerate('ČčĆćĐđŠšŽž'):
        s = s.replace(c, LETTERS[i])
    return s


def generate_oib():
    return ''.join(random.choices('0123456789', k=11))


def generate_users():
    for _ in range(NUM_USERS):
        name = random.choice(NAMES)
        surname = random.choice(SURNAMES)
        email_suffix = random.choice(DOMAINS)
        username = (_remove_diacritics(name) +
                    _remove_diacritics(surname)).lower() + str(random.randint(0, 1000))
        email = '{}@{}'.format(
            username, email_suffix)
        acc_id = auth.create_account(
            {'name': name, 'surname': surname, 'email': email, 'username': username, 'password': PASSWORD}, 'user')
        _users.append(acc_id)


def generate_sellers():
    for _ in range(NUM_SELLERS):
        base_name = random.choice(NAMES)
        name = random.choice(NAMES)
        surname = random.choice(SURNAMES)
        if random.random() > 0.5:
            base_name = name
        else:
            base_name = surname
        seller_name = '{} {}'.format(random.choice(SELLER_PREFIXES), base_name)
        oib = generate_oib()
        username = (_remove_diacritics(name) +
                    _remove_diacritics(surname)).lower() + str(random.randint(0, 1000))
        acc_id = auth.create_account(
            {'name': seller_name, 'oib': oib, 'password': PASSWORD, 'username': username}, 'seller')
        _sellers.append(acc_id)


def generate_tags():
    for i in range(NUM_TAGS):
        _tags.append(tags.new('tag' + str(i)))


def generate_price():
    return round(random.random() * 100 + 10, 2)


def generate_seller_products():
    s = set()

    for seller_id in _sellers:
        l = random.sample(PRODUCTS, k=5)
        for name in l:
            entry = product.get_by_name_min_price(name)
            if len(entry) == 0:
                product_id = product.new(name)
                _products.append(product_id)
                s.add(name)
            else:
                product_id = entry[0]['product_id']
            seller_product.new(product_id, seller_id, generate_price() * 100)
            _seller_products.append((seller_id, product_id))

    name = random.choice(list(set(PRODUCTS) - s))
    product_id = product.new(name)
    _products.append(product_id)

    for seller_id in _sellers:
        seller_product.new(product_id, seller_id, generate_price() * 100)
        _seller_products.append((seller_id, product_id))


def generate_price_changes():
    for seller_id, product_id in _seller_products:
        for _ in range(random.randint(2, 10)):
            seller_product.change_price(
                seller_id, product_id, generate_price() * 100)


def assign_tags():
    for product_id in _products:
        for user_id in random.sample(_users, k=10):
            for tag_id in random.sample(_tags, k=5):
                tags.add_tag_suggestion(user_id, product_id, tag_id)


def generate():
    generate_users()
    generate_sellers()
    generate_tags()
    generate_seller_products()
    assign_tags()
    generate_price_changes()
