from flask_wtf import FlaskForm
from wtforms import BooleanField, TextAreaField, SubmitField, IntegerField
from wtforms.validators import Length, DataRequired


class ProfilePrivacyForm(FlaskForm):
    user_id = IntegerField(validators=[DataRequired()])
    name = BooleanField()
    surname = BooleanField()
    email = BooleanField()


class CommentForm(FlaskForm):
    comment = TextAreaField(
        validators=[DataRequired(), Length(min=1, max=250)])
