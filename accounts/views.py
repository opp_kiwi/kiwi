import json

from flask import session, request, render_template, Blueprint, redirect, url_for, make_response
from flask.views import MethodView

from .. commons import check_tag_format
from .. database import tags, account, comments, seller, user, seller_product

from . import forms

from .. auth import require_levels

blueprint = Blueprint('accounts', __name__, url_prefix='/accounts')


class UserProfileView(MethodView):
    def get(self):
        user_id = request.args.get('user_id')
        user_info = user.get(user_id)
        privacy_info = user.get_privacy_settings(user_id)
        form = forms.ProfilePrivacyForm(data=privacy_info)
        return render_template('accounts/user_profile.html',
                               form=form, user=user_info, privacy=privacy_info)


class SellerProfileView(MethodView):
    def __init__(self):
        self.comment_form = forms.CommentForm()

    def get(self):
        seller_id = request.args.get('seller_id')
        seller_info = seller.get(seller_id)
        comments_ = comments.get_comments(seller_id=seller_id)
        products = seller_product.all_from(seller_id)
        return render_template('accounts/seller_profile.html',
                               comments=comments_, form=self.comment_form,
                               seller=seller_info, products=products)

    def post(self):
        seller_id = request.args.get('seller_id')
        user_id = session['id']
        if self.comment_form.validate_on_submit():
            comments.put_comment(
                seller_id=seller_id, user_id=user_id, comment=self.comment_form.comment.data)
        return redirect(request.url)


class AdminProfileView(MethodView):
    decorators = [lambda f: require_levels(f, ('admin',))]
    """ Admin panel view """

    def get(self):
        # Dispatch logic based on session arguments
        if session['account_type'] == 'admin':
            return render_template(
                'accounts/admin_panel.html',
                tags_count=tags.get_all_tags_count())
        else:
            return redirect(url_for('index'))

    def post(self):
        pass


class AdminManageTagsApi(MethodView):
    decorators = [lambda f: require_levels(f, ('admin',))]
    """ REST Api for frontend tag management """

    def post(self):
        """ Adds a new tag to a database.
            Reports an error in case of one. """

        tag_name = request.args.get('tag_name')

        # Implement error handling logic
        if tags.is_tag_name_used(tag_name):
            return make_response("Tag already exists", 409)

        tags.new(tag_name)

        return make_response("Tag added", 201)


class UpdatePrivacySettings(MethodView):
    decorators = [lambda f: require_levels(f, ('user',))]

    def post(self):
        form = forms.ProfilePrivacyForm()

        if form.validate_on_submit():
            user.update_privacy_info(form.user_id.data, form.name.data,
                                     form.surname.data, form.email.data)
            return redirect(url_for('accounts.user_profile', user_id=form.user_id.data))

        return redirect(url_for('index'))


blueprint.add_url_rule(
    '/mt_api', view_func=AdminManageTagsApi.as_view('admin_mt_api'))
blueprint.add_url_rule(
    '/profile', view_func=AdminProfileView.as_view('admin_profile'))
blueprint.add_url_rule(
    '/user_profile', view_func=UserProfileView.as_view('user_profile'))
blueprint.add_url_rule(
    '/seller_profile', view_func=SellerProfileView.as_view('seller_profile'))
blueprint.add_url_rule(
    '/update_privacy_settings', view_func=UpdatePrivacySettings.as_view('update_privacy_settings'))
