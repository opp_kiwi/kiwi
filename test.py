from . import database
from .database import account, product, tags, seller, seller_product
from flask import Flask
import unittest
import os
import datetime

class TestProducts(unittest.TestCase):

    def setUp(self):
        self.app = Flask(__name__, instance_relative_config=True)
        app = self.app
        index = app.instance_path.rfind('/', 0, app.instance_path.rfind('/'))
        app.instance_path = app.instance_path[:index+1] + 'instance'
        app.config.from_mapping(
            SECRET_KEY='DEV',
            ADMIN_USERNAME='admin',
            ADMIN_PASS='admin%pass',
            ADMIN_NAME='Glavni administrator',
            DATABASE=os.path.join(app.instance_path, 'db.sql')
        )
        if not os.path.exists(app.instance_path):
            try:
                os.makedirs(app.instance_path)
            except OSError as e:
                app.logger.error(f"Boot error: {e.strerror}")
        database.init_app(app)

    def tearDown(self):
        pass

    def test_product_add(self):
        self.app.app_context().push()
        seller_id = 145
        product_id = 0
        s = seller.get(seller_id)
        self.assertIsNot(s, None, "Make sure you ran `flask init-db; flask dummy-db`")
        product_names = ['product_0', 'product_1', 'product_2', '']
        for product_name in product_names:
            product_id = product.new(product_name)
            seller_product.new(product_id, seller_id, 1234)
            products = product.get_by_name(product_name)
            ids = []
            for i in products:
                ids.append(i[0])
            self.assertIn(product_id, ids)
        self.assertEqual([], product.get_by_name('some_name_that_doesnt_exist'))

    def test_tag_add(self):
        self.app.app_context().push()
        seller_id = 145
        product_id = 0
        s = seller.get(seller_id)
        tag_name_base = 'test_tag' + str(datetime.datetime.now())
        for i in range(20):
            tag_name = tag_name_base + str(i)
            self.assertFalse(tags.is_tag_name_used(tag_name))
            tag = tags.new(tag_name)
            self.assertTrue(tags.is_tag_name_used(tag_name))
            tags.add_tag_suggestion(seller_id, product_id, tag)
        pass

if __name__ == '__main__':
    unittest.main()
